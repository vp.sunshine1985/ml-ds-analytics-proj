# ML-DS-Analytics-Proj

**ML-DS-Analytics-Proj-Data Sources**


| Dataset | Definition |
| ------ | ------ |
| CarPrice_Prediction.csv | Provides details of cars,manufacturers and price details |
| churn_data.csv | Provides telecom data |
| Telecom Churn Data Dictionary.csv | Provides Telecom Churn Data Dictionary |
| customer_data.csv | Customer Data |
| Leads.csv | Provides Leads Data |
| Leads Data Dictionary.xlsx | Leads Data Dictionary |
| electric.csv | Datewise Electricity Production |
| sales-data.csv | Sales Data For Forcasting |
| exchange-rate-twi.csv | Monthwise Exchange Rate Data For Forcasting |
 

The data is provided in csv and xls format & most datasets are historical data sets. Few of them gets updated periodically.